import math

def computeComplexSquare(complex_number):
    real = (complex_number._re * complex_number._re) + (complex_number._im * complex_number._im) * (-1)
    imaginary = 2 * (complex_number._re * complex_number._im)
    square_complex_number = ComplexNumber(*(real, imaginary))
    return square_complex_number

class ComplexNumber():
    def __init__(self, re = 0.0, im = 0.0):
        self._re = re
        self._im = im
        self.tuple = (self._re, self._im)
        self.magnitude = 0.0
        self.magnitudeSquared = 0.0
        self._updateComputedProperties()

    def _computeMagnitude(self):
        return math.sqrt(self._re * self._re + self._im * self._im)

    def _computeMagnitudeSquared(self):
        return (self._re * self._re + self._im * self._im)

    def re(self):
        return self._re

    def setRe(self, newRe):
        self._re = newRe
        self._updateComputedProperties()

    def im(self):
        return self._im

    def setIm(self, newIm):
        self._im = newIm
        self._updateComputedProperties()

    def updateComplexNumber(self, newRe, newIm):
        self.setRe(newRe)
        self.setIm(newIm)

    def _updateComputedProperties(self):
        self.magnitude = self._computeMagnitude()
        self.magnitudeSquared = self._computeMagnitudeSquared() # (self._re * self._re + self.im * self.im)
        self.tuple = (self._re, self._im)


# Write a function to calculate the number of iterations after which a point escapes
# Test it with the example in the task description above.

def calculateNumOfIterationsBeforeEscape(number):
    x = 0
    iteration_counter = 0
# There was something that I was going wrong when I had these in the scope of the else statement (line 60)
    a = number._re
    b = number._im
    while x == 0 or x.magnitudeSquared <= 4:
        if x == 0:
            x = number
        else:
            square_complex = computeComplexSquare(x)
            x.setRe(square_complex._re + a)
            x.setIm(square_complex._im + b)
        iteration_counter += 1
    return iteration_counter


c1 = ComplexNumber(0.5, 0.5)
print(calculateNumOfIterationsBeforeEscape(c1))


