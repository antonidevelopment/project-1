import math


class C
{
    public:
        int re() const {
            return this->_re;
        }
        void setRe(int newReal) {
            this->_re = newReal;
            //any other ops
        }
    private:
        int _re;
        int _im;
}

class ComplexNumber():
    def __init__(self, re = 0.0, im = 0.0):
        self._re = re
        self._im = im
        self.magnitude = 0.0
        self.magnitudeSquared = 0.0
        self.square = (0.0, 0.0)
        self._updateComputedProperties()

    def _computeMagnitude(self):
        return math.sqrt(self._re * self._re + self._im * self._im)

    def _computeMagnitudeSquared(self):
        return (self._re * self._re + self._im * self._im)

    def _computeComplexSquare(self):
        real = (self._re*self._re) + (self._im*self._im)*(-1) #Is there a better name for this variable?
        imaginary = 2*(self._re * self._im)
        return (real, imaginary)

    def re(self):
        return self._re

    def setReal(self, newRe):
        self._re = newRe
        self._updateComputedProperties()

    def re(self):
        return self._re

    def setReal(self, newRe):
        self._re = newRe
        self._updateComputedProperties()

    def updateComplexNumber(self, newRe, newIm):


    def _updateComputedProperties(self):
        # when the re or im parts change, recompute magnitude, mag squared and square
        self.magnitude = self._computeMagnitude()
        self.magnitudeSquared = self._computeMagnitudeSquared()# (self._re * self._re + self.im * self.im)
        self.square = self._computeComplexSquare()



a = ComplexNumber(0.5, 0.5)
b = a.square
c = ComplexNumber(*b)
print(*b)
print(c.re, c.im)


#Write a function to calculate the number of iterations after which a point escapes
#Test it with the example in the task description above.

def calculateNumOfIterationsBeforeEscape(complex_number):
    x = 0
    iteration_counter = 0
    while x == 0 or x.magnitude <= 2:
        x[0] = (x.square[0] + complex_number.re)
        x[1] = (x.square[1] + complex_number.im)
        x.calculateMagnitude()
        iteration_counter += 1
    return x
